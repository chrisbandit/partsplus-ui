Parts Plus ui

This is the repository set up for testing all work on the new PartsPlus ui/frontend.
You will need 
node.js - https://nodejs.org/en/download/
git - windows (git-scm) https://git-scm.com/download/win
    - linux (ubuntu) sudo apt install git-all
    - linux (red hat or centos) yum install git 

Open up git-bash in windows or just bash in linux,

clone repository into your machine...
    git clone https://chrisbandit@bitbucket.org/chrisbandit/pp-ui.git

then install...
    npm install

then build the first time or when there are changes...
    npm build

then just start
    npm start

this will start a local web-server listening on 3001 (or whatever port is in the settings)
http://localhost:3001/

