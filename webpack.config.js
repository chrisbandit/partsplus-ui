var path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var postcssPresetEnv = require('postcss-preset-env');

module.exports = {
  devtool: 'source-map',
  mode: 'development',
  entry: [
    'babel-polyfill',
    'event-source-polyfill',
    './src/index',
    'webpack-hot-middleware/client'
  ],
  output: {
    filename: 'app.js',
    path: path.join(__dirname, 'dist'),
    publicPath: '/assets/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development'),
        'BABEL_ENV': JSON.stringify('development')
      },
      '__DEVTOOLS__': process.env.DEVTOOLS === 'true' ? true : false
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.template.html',
      favicon: path.join(__dirname, 'src/assets/images/favicon.ico')
    })
  ],
  module: { //TODO check out cacheDirectory setting for babel
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          { loader: 'postcss-loader', options: {
            ident: 'postcss',
            plugins: () => [
              postcssPresetEnv(/* options */)
            ]
          }}
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(jpg|png|gif|svg)$/,
        use: [
        {
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: './assets/',
            }
        }]
    }
    ]
  },
  resolve: {
    extensions: ['.js', '.css', '.png', '.woff', '.woff2', '.ttf', '.json']
  }
};